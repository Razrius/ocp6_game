import React, {Component} from "react";
import CombatManager from "./CombatManager";

class GameLoopManager extends Component {
  constructor(props) {
    super(props)
    this.state = {
      map: this.props.map,
      wallType: this.props.wallType,
      defaultWeapon: ["Default", "DS", "LinkToArtwork" , 10],
      dimension: this.props.dimension,
      p1Location: [],
      p1Health: 100,
      p1Weapon: [],
      p2Location: [],
      p2Health: 100,
      p2Weapon: [],
      currentPlayer: "P1",
      availableMoves: [],
      weapons: [],
      selectedCell: ["",0,0],
    }

    this.initPlayers = this.initPlayers.bind(this);
    this.initWeapons = this.initWeapons.bind(this);
    this.getPlayerLocationOnMap = this.getPlayerLocationOnMap.bind(this);
    this.setPlayerLocation = this.setPlayerLocation.bind(this);
    this.setCurrentPlayer = this.setCurrentPlayer.bind(this);
    this.getAdjecentTiles = this.getAdjecentTiles.bind(this);
    this.setAvailableMoves = this.setAvailableMoves.bind(this);
    this.getAvailableMoveLocations = this.getAvailableMoveLocations.bind(this);
    this.makeMove = this.makeMove.bind(this);
    this.setMapCellValue = this.setMapCellValue.bind(this);
    this.setEquippedWeapon = this.setEquippedWeapon.bind(this);
    this.checkEngageCombatMode = this.checkEngageCombatMode.bind(this);
    this.renderMap = this.renderMap.bind(this);
    this.ValidateMoveChoice = this.ValidateMoveChoice.bind(this);
    this.mapClick = this.mapClick.bind(this);
  }

  // On componentWillMount we initialize the players and the weapons in the state of this class
  componentWillMount() {
    this.initPlayers();
    this.initWeapons();
  }

  // Initializes the players and it's location on the map to the relevant state keys
  // It also assigns the default weapons to these players in the state
  // Everything is calculated based on the map generated in the parent component that was passed onto this class
  initPlayers() {
    this.setPlayerLocation("P1", this.getPlayerLocationOnMap("P1"));
    this.setState({ p1Weapon : this.state.defaultWeapon });
    this.setPlayerLocation("P2", this.getPlayerLocationOnMap("P2"));
    this.setState({ p2Weapon : this.state.defaultWeapon })
  }

  // Initializes the weapons in the state of this class. 
  // The weapon list was passed on from the parent component.
  initWeapons() {
    this.setState({ weapons: this.props.weapons });
  }

  // Returns the location on the map for the player that was passed on as an argument to the function
  // The returned value is in this format: [x,y]
  getPlayerLocationOnMap(props){
    let player = props;
    let location = [0,0];
    for (let i = 0; i < this.state.map.length; i++) {
      for (let j = 0; j < this.state.map.length; j++) {
        if (this.state.map[i][j] === player) {
          location = [i,j]
        }
      }
    }
    return location;
  }

  // Sets the value of currentPlayer in the state of this class to the one that was given as an argument
  setCurrentPlayer = (props) => {
    this.setState({ currentPlayer : props})
  }

  // Sets the the player's location in the state
  // Arguments are the player and it's location
  setPlayerLocation(player, location) {
    if (player === "P1") {
      this.setState({ p1Location: location });
    } else if ( player === "P2"){
      this.setState({ p2Location: location });
    }
  }

  // Checks all the available locations within a 3 steps radius around the given location paramater and returns an array with the result
  // The algorithm starts to "walk" into one direction until it either makes 3 steps or hits a wall.
  // the format for the returned array is : ["legend", [x,y]]
  getAvailableMoveLocations(player, location) {
    let dimension = this.state.dimension;
    let map = this.state.map;
    let surroundingTiles = [];
    let row = location[0];
    let col = location[1];

    let directions = [
      [[row - 1, col], [row - 2, col], [row - 3, col]],
      [[row + 1, col], [row + 2, col], [row + 3, col]],
      [[row, col + 1], [row, col + 2], [row, col + 3]],
      [[row, col - 1], [row, col - 2], [row, col - 3]],
    ];
    
    for (let i = 0; i < directions.length; i++) {
       
      let direction = directions[i];
       let hitWall = false;
       let j = 0;

       while ( j < direction.length && !hitWall) {

         let distance = direction[j];
         let x = distance[0];
         let y = distance[1];
 
         if ( x >= 0 && x < dimension && y >= 0 && y < dimension) {
           let legend = map[x][y];
           if (legend === this.state.wallType) {
             hitWall = true;
          } else if (legend !== this.state.wallType) {
            surroundingTiles.push([legend ,[x, y]])
          }
         }
         j++;
      }
    }
    return surroundingTiles;
  }

  // Stores the available moveset into the value of the availableMoves key in the state. 
  setAvailableMoves = (props) => {
    this.setState({ availableMoves : props });
  }

  // Returns an array with the contents of the tiles directly around the location passed as argument in this format : ["legend", [x,y]]
  getAdjecentTiles(location) {
    let dimension = this.state.dimension;
    let map = this.state.map;
    let loc = location;
    let row = loc[0];
    let col = loc[1];
    
    let directions = [
      [row - 1, col],
      [row + 1, col],
      [row, col + 1],
      [row, col - 1],
    ];
    
    let adjecentTileArray = [];

    for (let i = 0; i < directions.length; i++) {
      let x = directions[i][0];
      let y = directions[i][1];
      if (x >= 0 && x < dimension && y >= 0 && y < dimension) {
        let legend = map[x][y];
        adjecentTileArray.push([legend, [x,y]]);
      }
    }   
    return adjecentTileArray;
  }

  // This function executes the players choices for moving to a new cell
  // It also checks if target location is a weapon than it equips the weapon to the current player and drops the weapon to a nearby cell
  makeMove = (currentPlayer, currentLocation) => {
    let location = currentLocation;
    let player = currentPlayer;

    let targetCellRow = this.state.selectedCell[1];
    let targetCellCol = this.state.selectedCell[2];
    
    let currentWeapon = [];
    if (player === "P1") { 
      currentWeapon = this.state.p1Weapon;
    } else currentWeapon = this.state.p2Weapon;
    
    let targetCell = this.state.selectedCell;
    let adjecentToTargetCellTiles = this.getAdjecentTiles([targetCellRow, targetCellCol]);
    
    if (targetCell[0] != 0 && targetCell[0] != "P1" && targetCell[0] != "P2") {
      let counter = 0;
      let freeSpaceFound = false;
      do {
        if (adjecentToTargetCellTiles[counter][0] === 0) {
          freeSpaceFound = true;
          this.setMapCellValue(currentWeapon[1],[adjecentToTargetCellTiles[counter][1][0],adjecentToTargetCellTiles[counter][1][1]]);
        }
        counter++;
      } while (freeSpaceFound === false && counter < adjecentToTargetCellTiles.length);
      this.setEquippedWeapon(player,targetCell[0]);
    }

    this.setMapCellValue(0, location);
    this.setMapCellValue(player, [targetCellRow, targetCellCol]);
    this.setPlayerLocation(player, [targetCellRow, targetCellCol]);
    
    if (currentPlayer === "P1") {
          this.setCurrentPlayer("P2");
        } else {
          this.setCurrentPlayer("P1");
        }
  }

  // Sets a new value to any cell of the map that is stored in the state based on the parameters that was passed on.
  // It requires the new value of the cell and its location. 
  setMapCellValue = (legend, location) => {
    let cellValue = legend;
    let row = location[0];
    let col = location[1];
    let newMap = this.state.map;
    newMap[row][col] = cellValue;

    this.setState({ map : newMap});
  }

  // Sets the new weapon for the player that was given as an argument to this function.
  setEquippedWeapon = (currentPlayer, newWeaponLegend) => {
    let player = currentPlayer;
    let weaponLegend = newWeaponLegend;
    let newWeapon;
    let weaponList = this.state.weapons;
    for (let i = 0; i < weaponList.length; i++) {
      if (weaponLegend === weaponList[i][1]) {
        newWeapon = weaponList[i];
      } else if (newWeaponLegend === "DS"){
        newWeapon = this.state.defaultWeapon;
      }
    }
    
    if (player === "P1") {
      this.setState({ p1Weapon : newWeapon})
    } else if (player === "P2") {  
      this.setState({ p2Weapon : newWeapon})
    }
  }

  // Checks if Player 1 is adjecent to Player 2 returns a true or false value
  checkEngageCombatMode() {
    let combatMode = false;
    let adjecentTilesLegend = this.getAdjecentTiles(this.state.p1Location);
    for (let i = 0; i < adjecentTilesLegend.length; i++) {
      if (adjecentTilesLegend[i][0] === "P2") {
        combatMode = true;
      }
    }
    return combatMode;
  }

  ValidateMoveChoice(props) {
    let invalidChoice = true;
    let availableChoices = props;
    let selectedCell = this.state.selectedCell;
    let selectedRow = selectedCell[1];
    let selectedCol = selectedCell[2];
    
    availableChoices.map((props) => {      

      if (props[1][0] === selectedRow && props[1][1] === selectedCol) {
        invalidChoice = false;
      } return invalidChoice;
    })
    return invalidChoice;
  }

  mapClick = (e) => {
    let cellInfo = Array.from(e.target.dataset.value);
    let result = [];
    let tempValue = "";
    for(let i=0;i<cellInfo.length;i++) {
      if (cellInfo[i]!==",") {
        tempValue = tempValue + cellInfo[i];
      } else if (cellInfo[i] === ","){
        result.push(tempValue);
        tempValue = "";
      }      
    }
    result.push(tempValue);
    result[1] = parseInt(result[1]);
    result[2] = parseInt(result[2]);
    console.log(result);
    
    this.setState({
      selectedCell: result
    })
  }

  renderMap() {

    return (
      <div>
      <table className="map-table">
        <tbody>
        {this.state.map.map((props, indexRow) =>(
          <tr className="map-row" key={indexRow}>
          {props.map((props, indexCol) => (
            <td className={"cell"+props} id={"cell"+indexRow+indexCol} data-value={[props,indexRow,indexCol]} key={indexCol} onClick={(e) => this.mapClick(e)}>
              
            </td>
          ))}
          </tr>
        ))}
        </tbody>
      </table>
        
      </div>
    )
  }

  render() {
    let currentPlayer = this.state.currentPlayer;
    let currentLocation = this.getPlayerLocationOnMap(currentPlayer);
    let availableMoves = this.getAvailableMoveLocations(currentPlayer, this.getPlayerLocationOnMap(currentPlayer));

    let combatState = false;
    if (this.checkEngageCombatMode() === true) {
      combatState = true;
    }
    if (combatState === false) {
      
      return (
        <div>
            <div className="row">
            <div className="top-divider"></div>
            <div className="col s3">
              <div className="row">

                <div className="col s12">
                  <div className="card grey darken-3 side-panel">
                    <div className="card-content white-text">
                      <p className="player-title center"><i className="material-icons small">person</i> P1</p>
                      <p className="stats"><i className="material-icons small">location_on</i> R{this.state.p1Location[0] + 1} , C{this.state.p1Location[1] + 1} </p>
                      <p className="stats"><i className="material-icons small">favorite</i> {this.state.p1Health}</p>
                      <p className="stats"><i className="material-icons small">colorize</i> {this.state.p1Weapon[0]}</p>
                      <p className="stats"><i className="material-icons small">fitness_center</i> {this.state.p1Weapon[3]}</p>
                    </div>
                  </div>
                </div>

                <div className="col s12">
                  <div className="card grey darken-3 side-panel">
                    <div className="card-content white-text">
                      <p className="player-title center"><i className="material-icons small">person</i> P2</p>
                      <p className="stats"><i className="material-icons small">location_on</i> R{this.state.p2Location[0] + 1} , C{this.state.p2Location[1] + 1} </p>
                      <p className="stats"><i className="material-icons small pulse">favorite</i> {this.state.p2Health}</p>
                      <p className="stats"><i className="material-icons small">colorize</i> {this.state.p2Weapon[0]}</p>
                      <p className="stats"><i className="material-icons small">fitness_center</i> {this.state.p2Weapon[3]}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

              <div className="col s6">
                <div className="row">
                  {this.renderMap()}
                </div>
              </div>

              <div className="col s3">
                  <div className="col s12">
                    <div className="card grey darken-3 side-panel">
                      <div className="card-content white-text">
                        <p className="player-title center">Actions</p>
                        <p className="stats"><i className="material-icons small">person</i> {currentPlayer}</p>
                        <p className="stats"><i className="material-icons small">my_location</i> R{this.state.selectedCell[1] + 1}, C{this.state.selectedCell[2] + 1} </p>
                        <ul className="stats"> Available moves
                          {availableMoves.map((props, index) =>(
                          <li key={index} className="stats"> <i className="material-icons small">keyboard_arrow_right</i> R:{props[1][0] + 1 }, C:{props[1][1] + 1}</li>
                          ))}
                        </ul>
                      </div>
                      <div className="card-action center">
                    <button className="btn light-blue darken-1" disabled={this.ValidateMoveChoice(availableMoves)} onClick={() => this.makeMove(currentPlayer, currentLocation )}>Set Move</button>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
      )
    }
    else if (combatState === true) {
      return (
        <div>
          <CombatManager 
            p1Weapon={this.state.p1Weapon} 
            p2Weapon={this.state.p2Weapon}
          />
        </div>
      )
    }
  }
}

export default GameLoopManager;