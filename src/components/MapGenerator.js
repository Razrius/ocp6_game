import React, { Component } from "react";
import GameLoopManager from "./GameLoopManager";

class MapGenerator extends Component {
  constructor(props) {
  super(props)
  this.state = {
    isReady: false,
    dimension: this.props.dimension,
    mapDetails: {
    players: ["P1", "P2"],
    weaponPickUps: [
      ["Axe","A","linkToArtWork",26],
      ["Sword", "S", "linkToArtWork", 14],
      ["Daggers", "D", "linkToArtWork", 12],
      ["Bow", "B", "linkToArtWork", 19],
      ["Wand", "W", "linkToArtWork", 17],
      ["Mage Staff", "M", "linkToArtWork", 22],
      ["Halberd", "H", "linkToArtWork", 28],
    ],
    wallType: "X",
    },
    generatedMap: [],   
  }

  this.getRandomInt = this.getRandomInt.bind(this);
  this.generateMap = this.generateMap.bind(this);
  this.addWall = this.addWall.bind(this);
  this.addWeapon = this.addWeapon.bind(this);
  this.addPlayer = this.addPlayer.bind(this);
  this.setMap = this.setMap.bind(this);
  this.setMapState = this.setMapState.bind(this);
  }

  // While component loads start generating the blank map
  componentWillMount() {
  this.generateMap();
  }

  // Upon component mount start adding walls, weapons, and players to the map, once all done set map to be ready for loading in.
  componentDidMount() {
  this.addWall(this.state.generatedMap);
  this.addWeapon(this.state.generatedMap);
  this.addPlayer(this.state.generatedMap);
  this.setMapState(true);
  }

  // Updates the generated map based on the props it receives
  setMap = (props) => {
  this.setState({ generatedMap: props});
  }

  // Sets the flag for the map to be ready to be loaded
  setMapState = (props) => {
  this.setState({isReady : props})
  }

  // Generates an int between 0 and the argument. It rounds down the value.
  getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
  }

  // Generates the blank 2d map based on the dimensions stored in the state which was given as an argument to this component when it is called
  generateMap = () => {
  let dimension = this.state.dimension;
  let map = [];
  for (let i=0; i<dimension; i++) {
    let mapRow = [];
    for (let j = 0; j < dimension; j++) {
    mapRow.push(0);
    }
    map.push(mapRow);
  }
  this.setMap(map);
  } 

  // Adds the walls to the map, each row on the map can contain 0 to 3 walls 
  addWall = (props) => {
  let map = props;
  let dimension = this.state.dimension;

  for (let i = 0; i < dimension; i++) {
    let row = map[i];
    let wallCountPerRow = this.getRandomInt(4);
    for (let j = 0; j < wallCountPerRow; j++) {
    row[this.getRandomInt(this.state.dimension)] = this.state.mapDetails.wallType;
    }
  }
  this.setMap(map);
  }

  // Adds the weapons to the map. Each map can contain 0 to 4 weapons. It picks randomly from the weapon list that is stored in the state.
  // Multiple weapons of the same type can occur on the map. 
  // The function checks if the selected location contains a wall and if it is then it generates another random location and checks again until it finds a valid spot.
  addWeapon = (props) => {
  let map = props;
  // let weaponList = Object.values(this.state.mapDetails.weaponPickUps);
  let weaponList = this.state.mapDetails.weaponPickUps;

  for (let i=0; i <= this.getRandomInt(4); i++) {
    let currentWeapon = weaponList[this.getRandomInt(6)];
    
    let row, col, coordinates;
    do {
      row = this.getRandomInt(this.state.dimension);
      col = this.getRandomInt(this.state.dimension);
      coordinates = map[row][col];
    } while (coordinates === this.state.mapDetails.wallType);
    map[row][col] = currentWeapon[1];
  }
  this.setMap(map);
  }

  // Adds all of the players on the map by iterating through the players object in the state.
  // The function first finds a random location that it is not on the bordering cells.
  // It then checks if the surrounding cells contain another player, if yes the it repeats the process until both checks are valid
  addPlayer(props){
  let map = props;
  let dimension = this.state.dimension;
  this.state.mapDetails.players.forEach(element => {
    
    let row, col, coordinates, directions;
    let invalidLocation = true;
    let validCount = 0;
  
    while (invalidLocation && validCount < 4) {
    
    row = this.getRandomInt(this.state.dimension);
    col = this.getRandomInt(this.state.dimension);
    coordinates = map[row][col];
    
    directions = [
      [row - 1, col], 
      [row + 1, col], 
      [row, col + 1],
      [row, col - 1],
    ];

    if (coordinates === 0 && row >= 0 && row < dimension && col >= 0 && col < dimension) {
      validCount = 0;  
      for( let i=0; i < directions.length; i++) {
      let x = directions[i][0];
      let y = directions[i][1];
      
      if ( x < dimension  && x > 0 && y < dimension && y > 0) {
        if (map[x][y] === "P1") {
        invalidLocation = true;        
        } else {
        invalidLocation = false;
        validCount++;
        }
      }
      } 
    }
    }

    map[row][col] = element;
  });
  this.setMap(map);
  }

  // Render function invokes the game loop manager only when the map is ready to be passed on.
  render() {
    
    let mapState = this.state.isReady;
    if (mapState === false) {
      return (
      <div>
        It is loading, wait for it.
      </div>
      )
    }
    else if (mapState === true) {
      
      return (
      <div>
        <GameLoopManager 
          weapons={this.state.mapDetails.weaponPickUps} 
          map={this.state.generatedMap} 
          dimension={this.state.dimension} 
          wallType={this.state.mapDetails.wallType}/>
      </div>
      )
    }
  }
}

export default MapGenerator;