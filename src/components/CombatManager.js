import React, { Component } from "react";

class CombatManager extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      p1Health: 100,
      p1Weapon: [],
      p1AttackState: true,
      p2Health: 100,
      p2Weapon: [],
      p2AttackState: true,
      currentPlayer: "P1",
      combatOver: false,
      winner: "",
    }

    this.initPlayers = this.initPlayers.bind(this);
    this.setAttackState = this.setAttackState.bind(this);
    this.setPlayerHealth = this.setPlayerHealth.bind(this);
    this.setCurrentPlayer = this.setCurrentPlayer.bind(this);
    this.attack = this.attack.bind(this);
    this.defend = this.defend.bind(this);
  }

  // Initializes players before the component mounts
  componentWillMount() {
    this.initPlayers();
  }

  // Initializes players' weapon based on the arguments passed onto this component
  initPlayers = () => {
    this.setState({ p1Weapon: this.props.p1Weapon });
    this.setState({ p2Weapon: this.props.p2Weapon });
  }

  // Sets the AttackState of the player that is passed on as argument to this function.
  // AttackState defines whether the player wants to attack in his turn or defend
  setAttackState = (currentPlayer, attackState) => {
    let player = currentPlayer;
    let attackChoice = attackState;
    if ( player === "P1") {
      this.setState({ p1AttackState : attackChoice })
    } else if ( player === "P2") {
      this.setState({ p2AttackState: attackChoice })
    }
  }

  // Sets the health of the player who was passed on as argument to this function, with the required value
  setPlayerHealth = (currentPlayer, playerHealth) => {
    let player = currentPlayer;
    let health = playerHealth;
    if ( player === "P1") {
      this.setState({ p1Health: health })
    } else if ( player === "P2") {
      this.setState({ p2Health: health })
    }
  }

  // Sets the current player value in the state with the value that was passed on as an argument to this function
  setCurrentPlayer = (currentPlayer) => {
    let player = currentPlayer;
    this.setState({ currentPlayer : player});
  }

  // Calculates the health value of the enemy player based the attacking player's weaponDamage and if the enemy player defended or not in the previous turn.
  // It sets the health of the enemy player, then calls evaluateResults function to determine the outcome
  attack = (currentPlayer) => {

    let player = currentPlayer;
    let weaponDamage, enemyHealth, enemyAttackState;
    let defenseModifier = 0.5;
    this.setAttackState(player, true);

    if (player === "P1") {
      weaponDamage = this.state.p1Weapon[3];
      enemyHealth = this.state.p2Health;
      enemyAttackState = this.state.p2AttackState;
    } else if (player === "P2") {
      weaponDamage = this.state.p2Weapon[3];
      enemyHealth = this.state.p1Health;
      enemyAttackState = this.state.p1AttackState;
    }

    if (enemyAttackState === false) {
      enemyHealth = enemyHealth - weaponDamage * defenseModifier;
    } else if ( enemyAttackState === true) {
      enemyHealth = enemyHealth - weaponDamage;
    }

    if (player === "P1") {
      this.setPlayerHealth("P2", enemyHealth);
    } else if (player === "P2") {
      this.setPlayerHealth("P1", enemyHealth);
    }

    if (currentPlayer === "P1") {
      this.setCurrentPlayer("P2")
    } else if (currentPlayer === "P2") {
      this.setCurrentPlayer("P1")
    }
  }

  // Sets the attackState of the player to false, which means the player doesn't want to attack this turn and gains a 0.5 incoming damage modifier for the next turn.
  defend = (currentPlayer) => {
    let player = currentPlayer;
    this.setAttackState(player, false);

    if (currentPlayer === "P1") {
      this.setCurrentPlayer("P2")
    } else if (currentPlayer === "P2") {
      this.setCurrentPlayer("P1")
    }
  }
  
  render() {
    let combatOver = this.state.combatOver;
    let currentPlayer = this.state.currentPlayer;
    let winner = "";

    let p1Health = this.state.p1Health;
    let p2Health = this.state.p2Health;

    if (p1Health <= 0 || p2Health <= 0) {
      combatOver = true;
    }

    if (p1Health <= 0) {
      winner = "P2";
    } else if (p2Health <= 0) {
      winner = "P1";
    }

    if (combatOver === false) {
      return (
        <div className=" container">
          <div className="combat-divider"></div>
          <div className="row">
            <div className="col s12 m4">
              <div className="card grey darken-3">
                <div className="card-content white-text">
                  <p className="player-title center"><i className="material-icons small">person</i> P1</p>
                  <p className="stats"><i className="material-icons small">favorite</i> {this.state.p1Health}</p>
                  <p className="stats"><i className="material-icons small">colorize</i> {this.state.p1Weapon[0]}</p>
                  <p className="stats"><i className="material-icons small">fitness_center</i> {this.state.p1Weapon[3]}</p>
                </div>
              </div>
            </div>

            <div className="col s12 m4">
              <div className="card grey darken-3">
                <div className="card-content white-text">
                  <span className="card-title center">Combat Actions</span>
                  <p className="stats center"><i className="material-icons small">person</i> {currentPlayer}</p>
                </div>
                <div className="card-action center">
                  <button className="btn light-blue darken-1" onClick={() => this.attack(currentPlayer)}>Attack</button>
                  <button className="btn light-blue darken-1" onClick={() => this.defend(currentPlayer)}>Defend</button>
                </div>
              </div>
            </div>

            <div className="col s12 m4">
              <div className="card grey darken-3">
                <div className="card-content white-text">
                  <p className="player-title center"><i className="material-icons small">person</i> P2</p>
                  <p className="stats"><i className="material-icons small pulse">favorite</i> {this.state.p2Health}</p>
                  <p className="stats"><i className="material-icons small">colorize</i> {this.state.p2Weapon[0]}</p>
                  <p className="stats"><i className="material-icons small">fitness_center</i> {this.state.p2Weapon[3]}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
    else if (combatOver === true) {
      return (
        <div className="container combat-panel valign-wrapper">
          <div className="row">
            <div className="col s12 center-align">
              <div className="card">
                <div className="card-content">
                  <p className="player-title center">THE WINNER IS</p>
                  <p className="center winner-text">{winner}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
  }
}

export default CombatManager;