import React, { Component } from "react";

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      brandName : "OCP6 - Fantasy Wars - Board Game"
    }
  }

  render(){
    return (
      <header>
        <nav>
          <div class="nav-wrapper grey darken-3">
            <a href="/" class="brand-logo center">{this.state.brandName}</a>
          </div>
        </nav>
      </header>
    )
  }
}

export default Header;