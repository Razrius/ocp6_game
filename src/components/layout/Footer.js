import React, { Component } from "react";

class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  render() {
    return (
      <footer className="page-footer grey darken-3">
        <div className="container">
          <div className="row">
            <div className="col s6">
              <h5 className="white-text portfolio">Fantasy Wars</h5>
              <p className="grey-text text-lighten-4 footer-text">Front End Developer - Project 6</p>
            </div>
            <div className="col offset-s2 s4">
              <h5 className="white-text portfolio">Links</h5>
              <ul>
                <li><i className="material-icons">web</i><a className="grey-text text-lighten-3 portfolio" target="_blank" href="http://www.ferencmikola.com"> Portfolio</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright">
          <div className="container">
            © 2018 Ferenc Mikola
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer;