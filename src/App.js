import React, { Component } from "react";
import "./App.css";
import MapGenerator from "./components/MapGenerator";
import Header from "./components/layout/Header";
import Footer from "./components/layout/Footer";

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dimension: null,
      isReady: false,
      dimensionOptions: [10,12,14],
    }

    this.getDimension = this.getDimension.bind(this);
    this.handleDimensionSelection = this.handleDimensionSelection.bind(this);
  }

  getDimension() {
    return (
      <main>
        <div className="col s12 center-align">
          <div className="card">
            <div className="card-content">
              <span className="card-title center">Select Map Size</span>
              <p className="center center-align">Please select the board size below</p>
            </div>
            <div className="card-action center">
              {this.state.dimensionOptions.map((element,index) => (
                <button onClick={() => this.handleDimensionSelection(element)} className="light-blue darken-1 waves-effect waves-light btn map-selector-button" key={index}>
                  {element} x {element}
                </button>
              ))}
            </div>
          </div>
        </div>
      </main>
    )
  }

  handleDimensionSelection = (element) => {
    this.setState({
      dimension : element,
      isReady : true,
    })
  }

  render() {
    let dimensionState = this.state.isReady;
    if (dimensionState === false) {
      return (
        <div className="flexbox-wrapper">
          <Header />
          <main>
              <div className="container valign-wrapper map-selector-page">
                <div className="row">
                  {this.getDimension()}
                </div>
              </div>  
          </main>
          <Footer />
        </div>
      )
    }
    else if (dimensionState === true) {
      return (
        <div className="flexbox-wrapper">
          <Header />
          <main>
            <MapGenerator dimension={this.state.dimension} />
          </main>
          <Footer />
        </div>
      )
    }
  }
}

export default App;
